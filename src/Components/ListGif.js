import React from 'react'
import {List} from 'antd'
import ItemGif from '../Components/ItemGif'

function ListGif(props){
    return(
        <List grid={{ gutter: 16, column: 4}} dataSource={props.items} pagination={{defaultCurrent:1, pageSize:40}} renderItem = {item => (
            <List.Item>
                <ItemGif items = {item}/>
            </List.Item>
        )}/>
    )
}
export default ListGif