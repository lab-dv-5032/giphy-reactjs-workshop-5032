import React from 'react'
import { Card } from 'antd'
import { connect } from 'react-redux';

const { Meta } = Card

const mapDispatchToProps = dispatch => {
    return {
        onItemClick: item =>
            dispatch({
                type: 'click_item',
                payload: item
            })
    }
}

function ItemGif(props) {
    const items = props.items
    return (
        <Card
            cover={<img alt="example" src={items.images.fixed_width.url} style={{ height: '200px' }} />}
            hoverable
            onClick={() => {
                props.onItemClick(items);
            }}
        >
            <Meta
                title={items.title}
            >
            </Meta>

        </Card>
    )
}

export default connect(null, mapDispatchToProps)(ItemGif)