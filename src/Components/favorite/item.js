import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Card } from 'antd'
const { Meta } = Card;

const mapDispatchToProps = dispatch => {
    return {
        onItemGifClick: item =>
            dispatch({
                type: 'click_item',
                payload: item
            })
    }
}

class ItemFavorite extends Component {


    render() {
        const item = this.props.item
        return (
            <Card
                cover={<img alt="example" src={item.images.fixed_width.url} style={{ height: '200px'}} />}
                hoverable
    
            >
                <Meta
                    title={item.title}
                >
                </Meta>

            </Card>
        )
    }
}
export default connect(null, mapDispatchToProps)(ItemFavorite);