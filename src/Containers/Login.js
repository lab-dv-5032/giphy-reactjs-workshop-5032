import React, { Component } from 'react'
import {database, auth, provider} from '../firebase'
import { Form, Button, Input, Icon, message } from 'antd';

const KEY_USER_DATA = 'user'

class Login extends Component {

    state = {
        email: '',
        password: '',
        user: null
    }

    componentDidMount(){
        const jsonStr = localStorage.getItem(KEY_USER_DATA);
        const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;

        if(isLoggedIn){
            this.navigateToMainPage();
        }
    }
    navigateToMainPage = () => {
        const { history } = this.props;
        history.push('/gifs');
    }

    onClickLoginWithFB = () => {
        auth.signInWithPopup(provider).then(({ user }) => {
            this.setState({ user })
            localStorage.setItem(
                KEY_USER_DATA,
                JSON.stringify({
                    isLoggedIn: true,
                    email: user.email
                })
            );
            this.props.history.push('/gifs')
        })
    }

    onEmailChange = (event) => {
        const email = event.target.value
        this.setState({email})
    }

    onPasswordChange = (event) => {
        const password = event.target.value
        this.setState({password})
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    validatePassword(password) {
        var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
        return re.test(String(password));
    }

    onSubmitFormLogin = (event) => {
        event.preventDefault(); //ไม่ต้องรีเฟรชหน้า
        const isValid = this.validateEmail(this.state.email)
        const isValidPassword = this.validatePassword(this.state.password)
        if(isValid && isValidPassword){
            localStorage.setItem(
                KEY_USER_DATA,
                JSON.stringify({
                    isLoggedIn: true,
                    email: this.state.email
                })
            )
            this.navigateToMainPage();
            
        }else{
            //TODO: handle something wrong
            message.error('Email or Password invalid',1)
        }
    }

    render(){
        return(
            <div alignItem="center" style={{ width: '40%', justifyContent: 'center', alignItems:'center', margin:'auto'}}>
                <br/>
                <h1>Welcome to Giphy Searcher</h1>
                <br/>
                <h2>Login</h2>
                <Form onSubmit={this.onSubmitFormLogin}>
                    <Form.Item>
                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" onChange = {this.onEmailChange} />
                    </Form.Item>
                    <Form.Item>
                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" onChange = {this.onPasswordChange} />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType = "submit">Login</Button>
                    </Form.Item>
                </Form>
                <hr color= '#f2f2f2'/>
                <br/>
                <Button type="primary" icon="facebook" onClick = {this.onClickLoginWithFB}>Login with Facebook</Button>
                
            </div>
            
        )
    }
}
export default Login;