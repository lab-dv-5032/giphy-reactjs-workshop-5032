import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ListFavorite from '../Components/favorite/list'
import ListGif from '../Components/ListGif'

function RouteMenu(props) {
    return (
        <Switch>
            <Route path="/gifs" exact render={() => {
                return (
                    <ListGif items={props.items} />
                )
            }}
            />
            <Route path="/favorites" exact component={ListFavorite}/>
        </Switch>
    )
}
export default RouteMenu