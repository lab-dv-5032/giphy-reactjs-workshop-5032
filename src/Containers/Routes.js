import { Route, Switch } from 'react-router-dom'
import React from 'react'
import LoginPage from './Login'
import Main from './Main'
import ListFavorite from '../Components/favorite/list'


function Routes() {
    return (
        <div style={{ width: '100%' }}>
            <Switch>
                <Route exact path="/" component={LoginPage} />
                <Route component={Main} />
            </Switch>
        </div>
    )
}

export default Routes