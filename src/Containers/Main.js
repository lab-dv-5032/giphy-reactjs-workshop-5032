import React, { Component } from 'react';
import { Spin, Modal, Button, Layout, Menu, message, Input, Col, Row } from 'antd'
import RouteMenu from './RouteMenu'
import { connect } from 'react-redux';



const { Header, Content, Footer } = Layout;
const menus = ['gifs', 'favorites']
const Search = Input.Search;
const confirm = Modal.confirm;

const GphApiClient = require('giphy-js-sdk-core')
const client = GphApiClient("U9JjLBKXnJjekjHsZrq19uYZ54dbxG9W")

// const { history } = this.props;

const mapStateToProps = state => {
    return {
        isShowDialog: state.isShowDialog,
        itemGifClick: state.itemGifDetail
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onDismissDialog: () =>
            dispatch({
                type: 'dismiss_dialog'
            }),
        onItemClick: item =>
            dispatch({
                type: 'click_item',
                payload: item
            })
    }
}


class Main extends Component {

    state = {
        items: [],
        isShowModal: false,
        itemGif: null,
        pathName: menus[0],
        favItems: []
    }
    componentDidMount() {
        //list-fav
        const jsonStr = localStorage.getItem('list-fav')
        const items = JSON.parse(jsonStr) || []
        console.log(items)
        this.setState({ favItems:items })
        
        // // Trending Gifs
        fetch("http://api.giphy.com/v1/gifs/trending?&api_key=U9JjLBKXnJjekjHsZrq19uYZ54dbxG9W&limit=400")
            .then(response => response.json())
            .then(myJson => this.setState({ items: myJson.data }))
    }

    onMenuClick = e => {
        console.log(e.key)
        var path = '/'

        if (e.key !== '') {
            path = `${e.key}`
        }
        this.props.history.replace(path);
    }

    onModalClickCancle = () => {
        this.props.onDismissDialog();
    }

    onClickFavorite = () => {
        //TODO: save item to localStorage
        const itemClick = this.props.itemGifClick;
        const items = this.state.favItems

        const result = items.find(item => {
            return item.title === itemClick.title
        })
        //ถ้า item ที่เรากดหัวใจมีใน favItem มั้ย?
        if (result) {
            //TODO: show error
            message.error('Added to favorite', 1)
            this.onModalClickCancle()
        } else {
            //push item ที่เรากดหัวใจเข้า array favItem
            items.push(itemClick)
            // save to localStorage
            localStorage.setItem('list-fav', JSON.stringify(items))
            console.log(items)
            message.success('Saved to favorite', 1);
            //พอกดหัวใจแล้ว มันปิด modal detail หนังอัตโนมัติ
            this.onModalClickCancle()
        }

    }

    onClickShare = () => {
        const itemClick = this.props.itemGifClick;
        navigator.clipboard.writeText(itemClick.images.fixed_width.url)
        message.success('the gif url is copied to clipboard, Let\'s share it!', 1)
    }

    onSearch = (value) => {
        console.log(value)
        /// Gif Search
        client.search('gifs', { "q": value })
            .then((response) => {
                if (response.data.length !== 0) {
                    console.log('setState')
                    this.setState({ items: response.data })
                } else {
                    console.log('no data')
                    message.warning(`No Gifs found for ${value}`, 2)
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    onClickLogout = (e) => {
        confirm({
            title: 'Are you sure to Logout?',
            content: '',
            onOk() {
                console.log('OK');
                localStorage.setItem(
                    'user-data',
                    JSON.stringify({
                        isLoggedIn: false
                    })
                );
                // history.push('/')
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    showDialogConfirmLogout = () => {
        console.log('modal work')
        this.setState({ isShowModal: true });
    };

    handleCancel = () => {
        console.log('cancle click work')
        this.setState({ isShowModal: false });
    }

    handleOk = () => {
        console.log('logout work')
        this.setState({ isLoggedIn: false });
        localStorage.setItem(
            'user',
            JSON.stringify({
                isLoggedIn: false
            })


        );
        this.props.history.push('/');

        // setTimeout(() => {
        //     this.setState({ isLoading: false });

        // }, 1000);

    }


    render() {
        const item = this.props.itemGifClick
        console.log(item.images)
        return (
            <div>
                {this.state.items.length > 0 ? (
                    <div style={{ height: '100vh' }}>
                        {' '}
                        <Layout style={{ backgroundColor: 'white' }}>
                            <Header
                                style={{
                                    padding: '0px',
                                    position: 'fixed',
                                    zIndex: 1,
                                    width: '100%',
                                    alignItems: 'center',
                                    justifyItems: 'center'
                                }}
                            >
                                <Row type="flex" style={{ backgroundColor: 'white', alignItems: 'center', justifyItems: 'center' }}>
                                    <Col span={2} offset={1}>
                                        <h1>GiphySearcher</h1>
                                    </Col>
                                    <Col span={6} offset={3}>
                                        <Search placeholder="input search text" onSearch={value => { this.onSearch(value) }} enterButton style={{ padding: '20px' }} />
                                    </Col>

                                    <Col span={4} offset={4}>
                                        <Menu
                                            theme="light"
                                            mode="horizontal"
                                            defaultSelectedKeys={[this.state.pathName]}
                                            style={{ lineHeight: '64px' }}
                                            onClick={e => {
                                                this.onMenuClick(e);
                                            }}
                                        >
                                            <Menu.Item key={menus[0]}>Gifs</Menu.Item>
                                            <Menu.Item key={menus[1]}>Favorites</Menu.Item>

                                        </Menu>
                                    </Col>
                                    <Col span={2} offset={1}>
                                        <Button onClick={this.showDialogConfirmLogout}>Logout</Button>
                                        <Modal
                                            visible={this.state.isShowModal}
                                            onOk={this.handleOk}
                                            onCancel={this.handleCancel}
                                        >
                                            Are you sure to logout?
                                            </Modal>
                                    </Col>
                                </Row>
                            </Header>
                            <Content
                                style={{
                                    padding: '16px',
                                    marginTop: 64,
                                    minHeight: '600px',
                                    justifyContent: 'center', //จัดแนวนอน
                                    alignItems: 'center',//จัดแนวตั้ง
                                    display: 'flex'
                                }}
                            >
                                <RouteMenu
                                    items={this.state.items}
                                />
                            </Content>
                            <Footer style={{ textAlign: 'center', background: 'white' }}>
                                Giphy Application project @ CAMT
                        </Footer>
                        </Layout>
                    </div>
                ) : (
                        <div style={{ justifyContent: 'center', alignItems: 'center', position: 'fixed', top: '50%', left: '50%' }} >
                            <Spin tip="Loading..." size="large" />
                        </div>
                    )}
                {item !== null ? (
                    <Modal
                        width="40%"
                        style={{ maxHeight: '70%' }}
                        visible={this.props.isShowDialog}
                        onCancel={this.onModalClickCancle}
                        title={item.title}
                        footer={[
                            <Button
                                key="submit"
                                type="primary"
                                icon="heart"
                                size="large"
                                shape="circle"
                                onClick={this.onClickFavorite} />,
                            <Button
                                key="submit"
                                type="primary"
                                icon="share-alt"
                                size="large"
                                shape="circle"
                                onClick={this.onClickShare} />
                        ]}

                    >
                        {item.images != null ? (
                            <img alt={item.title} src={item.images.fixed_width.url} style={{ width: "100%" }} />
                        ) : (
                                <div />
                            )}

                    </Modal>
                ) : (
                        <div />
                    )}
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);