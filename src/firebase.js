import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDq3xWZ_yPV3T95w2maHtHASsKFo9LZXRM",
    authDomain: "workshop-dv-15820.firebaseapp.com",
    databaseURL: "https://example-everything.firebaseio.com",
    projectId: "workshop-dv-15820",
    storageBucket: "example-everything.appspot.com",
    messagingSenderId: "793947592819"
}

firebase.initializeApp(config)

const database = firebase.database()
const auth = firebase.auth()
const provider = new firebase.auth.FacebookAuthProvider()

export {
    database,
    auth,
    provider
}